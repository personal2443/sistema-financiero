from fastapi import FastAPI
from pydantic import BaseModel
from datetime import datetime
from src.routers.router import routers
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:4200"],  # Especifica aquí los orígenes permitidos
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
def home():
    return {"message": "Hello World"}

app.include_router(routers, prefix="/api")