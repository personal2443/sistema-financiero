from pydantic import BaseModel, Field
from datetime import datetime
from typing import Optional
from decimal import Decimal


class UsuarioSchema(BaseModel):
    id: Optional[int]
    cedula: int
    nombre: str
    clave: str

class CuentaSchema(BaseModel):
    id: Optional[int]
    cedula_usuario: int
    numero_cuenta: int
    saldo_cuenta: Decimal = Field(ge=0.01, decimal_places=2)

class transaccionSchema(BaseModel):
    id: Optional[int]
    Tipo_transaccion: str
    Cuenta_origen: int
    Cuenta_destino: Optional[int]
    Monto: Decimal = Field(ge=0.01, decimal_places=2)
    Fecha: datetime = datetime.now()



