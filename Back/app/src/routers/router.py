from fastapi import APIRouter
from src.models.model import UsuarioSchema, transaccionSchema, CuentaSchema
from src.services.service import service

routers = APIRouter()
Service = service()

# Ruta para obtener todos los usuarios
@routers.get("/usuario")
def get_user():
    usuarios = Service.get_all_user()
    return usuarios

@routers.get("/cuenta")
def get_account():
    cuenta = Service.get_all_account()
    return cuenta

@routers.get("/transaccion")
def get_transaction():
    transaccion = Service.get_all_transaction()
    return transaccion

# Ruta para crear un nuevo usuario
@routers.post("/usuario")
def create_user(user: UsuarioSchema):
    created_user = Service.create_user(user)
    return created_user

# Ruta para crear una nueva cuenta
@routers.post("/cuenta")
def create_account(cuenta: CuentaSchema):
    created_account = Service.create_account(cuenta)
    return created_account

# Ruta para crear una nueva transaccion
@routers.post("/transaccion")
def create_transaccion(transaccion: transaccionSchema):
    created_transaccion = Service.crear_transaccion(transaccion)
    return created_transaccion

# Ruta para obtener un usuario por su ID
@routers.get("/saldo/{item_id}")
def get_item(item_id: int):
    item = Service.get_saldo(item_id)
    if item:
        return item
    return {"error": "Item not found"}