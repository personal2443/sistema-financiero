from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker,aliased,outerjoin
from src.database.db import engine, Usuario, Transaccion, Cuenta
from src.models.model import UsuarioSchema, CuentaSchema, transaccionSchema
class service:
    def __init__(self):
        self.Session = sessionmaker(bind=engine)

    def get_all_user(self):
        session = self.Session()
        users = session.query(Usuario).all()
        session.close()
        return users

    def get_all_account(self):
        session = self.Session()
        users = session.query(Cuenta).all()
        session.close()
        return users

    def get_all_transaction(self):
        session = self.Session()
        cuenta_origen = aliased(Cuenta)
        cuenta_destino = aliased(Cuenta)

        query = session.query(
            Transaccion,
            cuenta_origen.numero_cuenta.label('cuenta_origen_numero'),
            cuenta_destino.numero_cuenta.label('cuenta_destino_numero')
        ).outerjoin(
            cuenta_origen,
            cuenta_origen.id == Transaccion.Cuenta_origen_id
        ).outerjoin(
            cuenta_destino,
            cuenta_destino.id == Transaccion.Cuenta_destino_id
        )

        # Agregar la función ifnull para manejar NULL en cuenta_destino
        query = query.add_columns(
            func.coalesce(cuenta_destino.numero_cuenta, None).label('cuenta_destino_numero')
        )

        resultados = query.all()
        print("🚀 ~ resultados:", resultados)
        session.close()

        lista_resultados = []
        for row in resultados:
            transaccion = row[0]
            cuenta_origen_numero = row[1]
            cuenta_destino_numero = row[2]
            resultado = {
                'id': transaccion.id,
                'tipo_transaccion': transaccion.Tipo_transaccion,
                'cuenta_origen': cuenta_origen_numero,
                'cuenta_destino': cuenta_destino_numero,
                'monto': transaccion.Monto,
                'fecha': transaccion.Fecha
            }
            lista_resultados.append(resultado)
        return lista_resultados

    def create_user(self, user: UsuarioSchema):
        session = self.Session()
        # Obtener la tabla 'usuarios' asociada a la clase Usuario
        usuarios_table = Usuario.__tablename__
        # Crear una instancia de Usuario mapeada a la tabla 'usuarios'
        usuario = Usuario()
        usuario.cedula = user.cedula
        usuario.nombre = user.nombre
        usuario.clave = user.clave
        # Agregar el objeto usuario a la sesión
        session.add(usuario)
        session.commit()
        session.close()
        return usuario

    def create_account(self, account: CuentaSchema):
        session = self.Session()
        # Obtener la tabla 'Cuenta' asociada a la clase Usuario
        usuarios_table = Cuenta.__tablename__
        # Crear una instancia de Usuario mapeada a la tabla 'usuarios'
        cuenta = Cuenta()
        cuenta.cedula_usuario = account.cedula_usuario
        cuenta.numero_cuenta = account.numero_cuenta
        cuenta.saldo_cuenta = account.saldo_cuenta
        # Agregar el objeto usuario a la sesión
        session.add(cuenta)
        session.commit()
        session.close()
        return cuenta

    def crear_transaccion(self, transaccion: transaccionSchema):
        # Obtener datos del formulario o solicitud JSON
        tipo_transaccion = transaccion.Tipo_transaccion
        cuenta_origen = transaccion.Cuenta_origen
        cuenta_destino = transaccion.Cuenta_destino
        monto = transaccion.Monto
        fecha = transaccion.Fecha
        # Crear una nueva sesión de base de datos
        session = self.Session()

        try:
            # Buscar las cuentas de origen y destino
            data_origen = session.query(Cuenta).filter(Cuenta.numero_cuenta == cuenta_origen).first()

            if tipo_transaccion == "trasnfer":
                data_destino = session.query(Cuenta).filter(Cuenta.numero_cuenta == cuenta_destino).first()
                if data_destino is None:
                    return 'No se encontró la cuenta de destino'
                else:
                    destino = data_destino.id
            else:
                destino = None

            if data_origen is None:
                return 'No se encontró la cuenta de origen'

            if data_origen.saldo_cuenta < monto :
                return 'Saldo insuficiente'

            # Crear una nueva transacción
            transaccion = Transaccion(Tipo_transaccion=tipo_transaccion, Cuenta_origen_id=data_origen.id, Cuenta_destino_id=destino, Monto=monto, Fecha=fecha)
            # Agregar la transacción a la sesión
            session.add(transaccion)

            # Actualizar los saldos de las cuentas
            if tipo_transaccion == "consigned":
                data_origen.saldo_cuenta += monto
            if tipo_transaccion == "withdraw":
                data_origen.saldo_cuenta -= monto
            if tipo_transaccion == "trasnfer":
                data_origen.saldo_cuenta -= monto
                data_destino.saldo_cuenta += monto

            # Guardar los cambios en la base de datos
            session.merge(data_origen)
            if tipo_transaccion == "trasnfer":
                session.merge(data_destino)
            session.commit()
            return 'Transacción creada exitosamente'
        except Exception as e:
            # Revertir cambios en caso de error
            session.rollback()
            return f'Error al crear transacción: {str(e)}'
        finally:
            # Cerrar la sesión de base de datos
            session.close()

    def get_saldo(self, cuenta: int):
        try:
            session = self.Session()
            cuenta = session.query(Cuenta).filter_by(numero_cuenta=cuenta).one()
            usuario = cuenta.usuario
            return {"nombre": usuario.nombre, "cuenta":cuenta.numero_cuenta, "saldo": cuenta.saldo_cuenta}
        except Exception as e:
            # Revertir cambios en caso de error
            session.rollback()
            return f'Error al consultar: {str(e)}'
        finally:
            # Cerrar la sesión de base de datos
            session.close()
