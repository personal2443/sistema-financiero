from sqlalchemy import Column, Integer, DECIMAL, TIMESTAMP, ForeignKey, String, text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine


Base = declarative_base()

class Usuario(Base):
    __tablename__ = 'usuarios'

    id = Column(Integer, primary_key=True)
    cedula = Column(Integer, nullable=False, unique=True)
    nombre = Column(String(100), nullable=False)
    clave = Column(String(100), nullable=False)


class Cuenta(Base):
    __tablename__ = 'cuenta'

    id = Column(Integer, primary_key=True)
    saldo_cuenta = Column(DECIMAL(10, 2), nullable=False)
    numero_cuenta = Column(Integer, nullable=False, unique=True)
    cedula_usuario = Column(Integer, ForeignKey('usuarios.cedula'))

    usuario = relationship("Usuario", foreign_keys=[cedula_usuario])

    def __repr__(self):
        return f"Cuenta(id={self.id}, Cedula_usuario={self.cedula_usuario}, Saldo_Cuenta={self.saldo_cuenta}, Numero_Cuenta={self.numero_cuenta})"



class Transaccion(Base):
    __tablename__ = 'transacciones'

    id = Column(Integer, primary_key=True)
    Tipo_transaccion = Column(String(100), nullable=False)
    Cuenta_origen_id = Column(Integer, ForeignKey('cuenta.id'), nullable=False)
    Cuenta_destino_id = Column(Integer, ForeignKey('cuenta.id'), nullable=True)
    Monto = Column(DECIMAL(10, 2), nullable=False)
    Fecha = Column(TIMESTAMP, server_default=text('CURRENT_TIMESTAMP'))

    Cuenta_origen = relationship("Cuenta", foreign_keys=[Cuenta_origen_id])
    Cuenta_destino = relationship("Cuenta", foreign_keys=[Cuenta_destino_id])

    def __repr__(self):
        return f"Transaccion(id={self.id}, Cuenta_origen_id={self.Cuenta_origen_id}, Cuenta_destino_id={self.Cuenta_destino_id}, Monto={self.Monto}, Fecha={self.Fecha})"

# Crear la tabla en la base de datos
engine = create_engine('postgresql://postgres:SisFin2023@db-sistema-financiero.ccmzr7fkmzyr.us-east-1.rds.amazonaws.com:5432/bd_sistema_financiero')
# metadrop = Base.metadata.drop_all(engine)
meta = Base.metadata.create_all(engine)
