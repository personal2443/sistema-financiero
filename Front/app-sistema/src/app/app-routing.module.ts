import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { TransaccionesComponent } from './transacciones/transacciones.component'
import { UsuariosComponent } from './usuarios/usuarios.component';
import { CuentasComponent } from './cuentas/cuentas.component';

const routes: Routes = [
  {
    path: 'transacciones', component: TransaccionesComponent,
    data: {
      title: 'Transacciones',
      animation: 'FilterPage',
    },
  },

  {
    path: 'cuentas', component: CuentasComponent,
    data: {
      title: 'Cuentas',
      animation: 'FilterPage',
    },
  },

  {
    path: 'usuarios', component: UsuariosComponent,
    data: {
      title: 'Usuarios',
      animation: 'FilterPage',
    },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
