import { Component } from '@angular/core';
import { ServiciosService } from '../service/servicios.service'
import { MatDialog } from '@angular/material/dialog';
import { UsuarioDialogComponent } from 'app/usuario-dialog/usuario-dialog.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent {
  displayedColumns: string[] = ['id', 'nombre', 'cedula'];
  dataSource: any;

  constructor(private service: ServiciosService,
    public dialog: MatDialog){}

  ngOnInit() {
    this.loadData()
  }

  loadData() {
    this.service.getusuarios().subscribe(res => {
      this.dataSource = res
    }, err => {
      console.log("ERROR",err)
    })
  }

  openDialogUsuario(): void {
    const dialogRef = this.dialog.open(UsuarioDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.postusario(result).subscribe(res => {
          this.loadData()
        }, err => {
          console.log("ERROR",err)
        })
      }
    });
  }
}
