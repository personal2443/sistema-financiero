import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-cuenta-dialog',
  templateUrl: './cuenta-dialog.component.html',
  styleUrls: ['./cuenta-dialog.component.scss']
})
export class CuentaDialogComponent {

  cedula_usuario: number;
  numero_cuenta: number;
  saldo_cuenta: number;

  constructor(private dialogRef: MatDialogRef<CuentaDialogComponent>) { }

  cancel(): void {
    this.dialogRef.close();
  }

  enviarFormulario() {
    return {
      cedula_usuario: this.cedula_usuario,
      numero_cuenta: this.numero_cuenta,
      saldo_cuenta: this.saldo_cuenta
    };
  }
}
