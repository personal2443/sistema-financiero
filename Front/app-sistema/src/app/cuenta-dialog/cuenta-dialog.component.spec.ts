import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CuentaDialogComponent } from './cuenta-dialog.component';

describe('CuentaDialogComponent', () => {
  let component: CuentaDialogComponent;
  let fixture: ComponentFixture<CuentaDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CuentaDialogComponent]
    });
    fixture = TestBed.createComponent(CuentaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
