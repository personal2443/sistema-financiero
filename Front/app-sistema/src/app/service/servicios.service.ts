import { Injectable } from '@angular/core';
// import { RequestService } from '../service/request.service'
import { HttpClient } from "@angular/common/http";
import { RequestModel } from '../models/request.model'
import { EndPointsApp } from '../util/const';
import { CuentasFilterInterface } from '../models/cuentas.model';
import { CuentasResponseInterface } from '../models/cuentas-response.interface';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {
  rutaApi = "http://localhost:8000/api"
  
  constructor(
    private http: HttpClient,
  ) { }

  getcuentas() {
    return this.http.get(`${this.rutaApi}/cuenta`);
  }

  getusuarios() {
    return this.http.get(`${this.rutaApi}/usuario`);
  }

  gettransferencia() {
    return this.http.get(`${this.rutaApi}/transaccion`);
  }

  getsaldo(cuenta) {
    return this.http.get(`${this.rutaApi}/saldo/${cuenta}`);
  }

  postusario(formularioData) {
    return this.http.post(`${this.rutaApi}/usuario`,formularioData);
  }

  postcuenta(formularioData) {
    return this.http.post(`${this.rutaApi}/cuenta`,formularioData);
  }

  posttransferencia(formularioData) {
    return this.http.post(`${this.rutaApi}/transaccion`,formularioData);
  }
}
