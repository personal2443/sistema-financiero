export enum EndPointsApp {
    mainUrl = 'http://localhost:3200/',
    transacciones = "transacciones/",
    cuentas = "cuentas/",
    usuarios = "usuarios/",
}

export enum Endpoints {
    mainUrl = 'http://localhost:3200/',
}