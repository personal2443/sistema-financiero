import { Component, OnInit, AfterViewInit, ViewChild} from '@angular/core';
import { CuentasItemResponseInterface } from '../models/cuentas-response.interface';
import { MatTable,MatTableDataSource,MatTableModule } from '@angular/material/table';
import { CUENTAS_MAIN_DATATABLE_COLUMNS } from '../models/cuentas.model';
import { ServiciosService } from '../service/servicios.service'
import { MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
@Component({
  selector: 'app-transacciones',
  templateUrl: './transacciones.component.html',
  styleUrls: ['./transacciones.component.scss']
})
export class TransaccionesComponent {
  displayedColumns: string[] = ['id', 'Tipo_transaccion', 'Cuenta_origen_id', 'Cuenta_destino_id','Monto', 'Fecha'];
  dataSource: any;

  constructor(private service: ServiciosService){}

  ngOnInit() {
    this.loadData()
  }

  loadData() {
    this.service.gettransferencia().subscribe(res => {
      this.dataSource = res
    }, err => {
      console.log("ERROR",err)
    })
  }
}