import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-usuario-dialog',
  templateUrl: './usuario-dialog.component.html',
  styleUrls: ['./usuario-dialog.component.scss']
})
export class UsuarioDialogComponent {
  cedula: number;
  nombre: string;
  clave: string;

  constructor(private dialogRef: MatDialogRef<UsuarioDialogComponent>) { }

  cancel(): void {
    this.dialogRef.close();
  }

  enviarFormulario() {
    return {
      cedula: this.cedula,
      nombre: this.nombre,
      clave: this.clave
    };
  }
}
