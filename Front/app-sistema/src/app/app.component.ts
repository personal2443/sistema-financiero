import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component';
import { ServiciosService } from './service/servicios.service';
import { AccountDialogComponent } from './account-dialog/account-dialog.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app-sistema';

  constructor(
    private dialog: MatDialog,
    private service: ServiciosService) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '400px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
//         value="trasnfer" \\ value="consigned" \\ value="withdraw" \\ value="checkBalance"
        console.log("🚀 ~ dialogRef.afterClosed ~ result:", result)
        if (result.Tipo_transaccion == 'checkBalance') {
          this.service.getsaldo(result.Cuenta_origen).subscribe(res => {
            const dialogRef = this.dialog.open(AccountDialogComponent, {
              data: res
            });
          }, err => {
            console.log("ERROR",err)
          })
        } else {
          this.service.posttransferencia(result).subscribe(res => {
            window.location.reload();
          }, err => {
            console.log("ERROR",err)
          })
        }
      }
    });
  }
}
