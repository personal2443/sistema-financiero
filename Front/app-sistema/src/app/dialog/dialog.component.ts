import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  selectedAction: string;
  account: string;
  destinationAccount: string;
  amount: number;

  constructor(private dialogRef: MatDialogRef<DialogComponent>) { }

  ngOnInit(): void {
  }

  cancel(): void {
    this.dialogRef.close();
  }

  getFormValues(): any {
    return {
      Tipo_transaccion: this.selectedAction,
      Cuenta_origen: this.account,
      Cuenta_destino: this.destinationAccount ?? null,
      Monto: this.amount
    };
  }
}