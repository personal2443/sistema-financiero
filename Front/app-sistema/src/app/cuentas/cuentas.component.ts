import { Component, OnInit} from '@angular/core';
import { ServiciosService } from '../service/servicios.service'
import { CuentaDialogComponent } from 'app/cuenta-dialog/cuenta-dialog.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-cuentas',
  templateUrl: './cuentas.component.html',
  styleUrls: ['./cuentas.component.scss']
})

export class CuentasComponent  implements OnInit{
  displayedColumns: string[] = ['id', 'numero_cuenta', 'cedula_usuario','saldo_cuenta'];
  dataSource: any;

  constructor(private service: ServiciosService,
    public dialog: MatDialog){}

  ngOnInit() {
    this.loadData()
  }

  loadData() {
    this.service.getcuentas().subscribe(res => {
      this.dataSource = res
    }, err => {
      console.log("ERROR",err)
    })
  }

  openDialogCuenta(): void {
    const dialogRef = this.dialog.open(CuentaDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.service.postcuenta(result).subscribe(res => {
          this.loadData()
        }, err => {
          console.log("ERROR",err)
        })
      }
    });
  }

}