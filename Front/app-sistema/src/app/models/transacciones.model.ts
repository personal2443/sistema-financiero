export interface TransaccionesFilterInterface{
    id: number
    cuentaorigen: number
    cuentadestino: number
    monto: number
    fecha: Date
    fecha_fiñal_date: Date
}
export enum transaccionesFormEnum{
    id = "id",
    cuentaorigen = "cuentaorigen",
    cuentadestino = "cuentadestino",
    monto = "monto",
    fecha = "fecha",
    fecha_fiñal_date = "fecha_fiñal_date",
}
export const TRANSACCIONES_MAIN_DATATABLE_COLUMNS = ['id','cuentaorigen','cuentadestino','monto','fecha']

