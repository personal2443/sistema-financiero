export interface CuentasResponseInterface {
    items: CuentasItemResponseInterface[]
    rowCount: string
}

export interface CuentasItemResponseInterface {
    id: number
    saldo: number
    numerocuenta: number
    cedulausuario: number

}

export interface excelCuentasInterface extends CuentasItemResponseInterface {
    accion: string
}

export interface GenericResponseInterface {
    hasMore: boolean
    limit: number
    offset: number
    count: number
    links: GenericResponseLinksInterface[]
    items: any[]
  
  }
  
   interface GenericResponseLinksInterface {
    rel: string
    href: string
  }
