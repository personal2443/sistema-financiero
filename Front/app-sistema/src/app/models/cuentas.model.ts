export interface CuentasFilterInterface{
    id: number
    saldo: number
    numerocuenta: number
    cedulausuario: number
}
export enum cuentasFormEnum{
    id = "id",
    saldo = "saldo",
    numerocuenta = "numerocuenta",
    cedulausuario = "cedulausuario",
}
export const CUENTAS_MAIN_DATATABLE_COLUMNS = ['id','saldo','numerocuenta','cedulausuario']

