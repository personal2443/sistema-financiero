export interface UsuariosFilterInterface{
    id: number
    cedula: number
    nombre: string
    clave: string
}
export enum usuariosFormEnum{
    id = "id",
    cedula = "cedula",
    nombre = "nombre",
    clave = "clave",
}
export const USUARIOS_MAIN_DATATABLE_COLUMNS = ['id','cedula','nombre','clave']

