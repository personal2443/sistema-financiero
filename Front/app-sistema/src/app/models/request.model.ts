export class RequestModel {

  endpoint: string
  data: any
  stateId: string
  params: any
  headers: any


  constructor(
    endpoint: string,
    data: any,
    stateId: string,
    params?: any,
    headers?: any,

  ) {

    this.endpoint = endpoint
    this.data = data
    this.stateId = stateId
    this.params = params
    this.headers = headers
  }

}
