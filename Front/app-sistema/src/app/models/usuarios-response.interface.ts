export interface UsuariosResponseInterface {
    items: UsuariosItemResponseInterface[]
    rowCount: string
}

export interface UsuariosItemResponseInterface {
    id: number
    cedula: number
    nombre: string
    clave: string

}

export interface excelUsuariosInterface extends UsuariosItemResponseInterface {
    accion: string
}
