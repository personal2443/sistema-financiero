export interface TransaccionesResponseInterface {
    items: TransaccionesItemResponseInterface[]
    rowCount: string
}

export interface TransaccionesItemResponseInterface {
    id: number
    cuentaorigen: number
    cuentadestino: number
    monto: number
    fecha: Date

}

export interface excelTransaccionesInterface extends TransaccionesItemResponseInterface {
    accion: string
}
